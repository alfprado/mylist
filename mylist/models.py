from sqlalchemy import Column, Integer, String, Boolean
from .database import Base

class Product(Base):
    __tablename__ = "products"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    description = Column(String)
    link = Column(String)
    picture = Column(String)
    is_active = Column(Boolean, default=True)
