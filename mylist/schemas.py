from pydantic import BaseModel
from typing import Optional


class ProductBase(BaseModel):
    title: str
    description: str = None
    link: str = None
    picture: str = None

class Product(ProductBase):
    class Config:
        orm_mode = True

class ShowProduct(BaseModel):
    title: str
    description: str = None
    link: str = None
    picture: str = None
    is_active: Optional[bool] = None
    class Config:
        orm_mode = True