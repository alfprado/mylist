from fastapi import FastAPI
from . import models
from .database import engine
from .routers import product
import uvicorn

app = FastAPI()

models.Base.metadata.create_all(engine)

app.include_router(product.router)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)