from fastapi import APIRouter, Depends, status
from .. import models, database, schemas
from sqlalchemy.orm import Session
from ..repository import product
from typing import List

router = APIRouter(
    prefix='/product',
    tags=['Products']
)

get_db = database.get_db

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: schemas.ShowProduct, db: Session = Depends(get_db)):
    return product.create(request, db)

@router.get('/', response_model=List[schemas.ShowProduct])
def all(db: Session = Depends(get_db)): 
    return product.get_all(db)

@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
def destroy(id: int, db: Session = Depends(get_db)):
    return product.destroy(id, db)

@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id: int, request: schemas.ShowProduct, db: Session = Depends(get_db)):
    return product.update(id, request, db)

@router.get('/{id}', status_code=200, response_model=schemas.ShowProduct)
def show(id: int, db: Session = Depends(get_db)):
    return product.show(id, db)

@router.get('/random', status_code=200, response_model=schemas.Product)
def random(db: Session = Depends(get_db)):
    return product.random(db)