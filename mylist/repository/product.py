from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import func
from .. import models, schemas
from fastapi import HTTPException, status

def get_all(db: Session):
    products = db.query(models.Product).all()
    return products

def create(request: schemas.Product, db: Session):
    new_product = models.Product(
        title = request.title, 
        description = request.description, 
        link = request.link,
        picture = request.picture,
        is_active = request.is_active)
    db.add(new_product)
    db.commit()
    db.refresh(new_product)
    return new_product

def destroy(id: int, db: Session):
    product = db.query(models.Product).filter(models.Product.id == id)
    if not product.first():
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f'Product with id {id} not found') 

    product.delete(synchronize_session = False)
    db.commit()
    return 'done'

def update(id: int, request: schemas.Product, db: Session):
    product = db.query(models.Product).filter(models.Product.id == id)
    if not product.first():
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f'Product with id {id} not found')
    product.update({
        'title': request.title, 
        'description': request.description,
        'link': request.link,
        'picture': request.picture,
        'is_active': request.is_active
        })
    db.commit()
    return 'updated'

def show(id: int, db: Session):
    product = db.query(models.Product).filter(models.Product.id == id).first()
    if not product:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f'Product with the id {id} is not available')
    return product

def random(db: Session):
    product = db.query(models.Product).filter(models.Product.is_active == True).order_by(func.random()).first()
    if not product:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                            detail = f'Product is not available')
    return product