# WishList (Lista de Desejos)

| Método | Endpoint |  |
|-----|---------|-----|
| GET | /product | Lista produtos |
| GET | /product/{id} | Exibe produto |
| GET | /product/random | Exibe aleatório (somente os ativos) |
| POST | /product/{id} | Cria produto |
| PUT | /product/{id} | Atualiza produto |
| DELETE | /product/{id} | Deleta produto |

## Instalação

```sh
$ git clone https://gitlab.com/alfprado/mylist.git

$ cd mylist

$ pipenv install

$ pipenv shell
```

Crie um arquivo .env na raiz do projeto com o seguinte conteúdo:

DATABASE_URL = postgresql+psycopg2://postgres:postgres@db:5432

```sh
$ docker-compose build

$ docker-compose up
```

## Documentação

```sh
localhost:8000/docs
```

## License

MIT